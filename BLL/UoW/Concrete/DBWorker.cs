﻿using BLL.UoW.Interface;
using DAL.AppDataContext;
using DAL.Repository.Concrete;
using DAL.Repository.Interface;

namespace BLL.UoW.Concrete
{
    public class DBWorker : IDBWorker
    {
        private IMessageRepository messageRepository;
        private IUserRepository userRepository;
        private IProductRepository productRepository;

        public IMessageRepository ChatRepository
        {
            get
            {
                if (messageRepository == null)
                    messageRepository = new MessageRepository(DataContext.Instance);
                return messageRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(DataContext.Instance);
                return userRepository;
            }
        }

        public IProductRepository ProductRepository
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(DataContext.Instance);
                return productRepository;
            }
        }


    }
}
