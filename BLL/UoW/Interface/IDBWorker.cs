﻿using DAL.Repository.Interface;

namespace BLL.UoW.Interface
{
    public interface IDBWorker
    {
        IMessageRepository ChatRepository { get; }
        IUserRepository UserRepository { get; }
        IProductRepository ProductRepository { get; }
    }
}
