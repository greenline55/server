﻿using DAL.Entity;
using DAL.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Service
{

    [ServiceContract]
    public interface IAPIService
    {

        //[OperationContract]
        //[WebGet]
        //IEnumerable<ItemMessage> GetAllMessage();

        //[OperationContract]
        //[WebInvoke]
        //void SendMessage(string userName, string message);

        [OperationContract]
        [WebGet]
        Guid Login(string userName);

        [OperationContract]
        [WebGet]
        UserProfile GetUser(string userId);

        [OperationContract]
        [WebInvoke]
        Guid SaveUser(string name, string address, string coordX, string coordY, string skype, string mail, string phone, string description, string userType);

        [OperationContract]
        [WebGet]
        IEnumerable<ProductMapper> GetAllProducts();

        [OperationContract]
        [WebGet]
        IEnumerable<ProductMapper> GetProducts(string userId);

        [OperationContract]
        [WebGet]
        ProductMapper GetProduct(string productId);

        [OperationContract]
        [WebInvoke]
        string UploadFile(Stream file);

        [OperationContract]
        [WebInvoke]
        Guid AddProduct(string name, string description, double voluem, double price, string unitType, string photoPath, string vendorId);

    }

}
