﻿using BLL.UoW.Concrete;
using BLL.UoW.Interface;
using DAL.Entity;
using DAL.Mapper;
using DAL.Model.Enums;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.IO;

namespace Service
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class APIService : IAPIService
    {
        private IDBWorker dbWorker = null;

        public APIService()
        {
            dbWorker = new DBWorker();
        }

        public UserProfile GetUser(string userId)
        {
            Guid id = new Guid(userId);
            return dbWorker.UserRepository.GetUser(id);
        }

        public Guid SaveUser(string name, string address,string coordX, string coordY, string skype, string mail, string phone, string description, string userType)
        {
            UserTypeEnum type = (UserTypeEnum)Enum.Parse(typeof(UserTypeEnum), userType);
            TCoord coord = new TCoord { X = coordX, Y = coordY };

            TUser user = new TUser
            {
                Name = name,
                Address = address,
                Coord=coord,
                Phone = phone,
                Skype=skype,
                Mail= mail,

                Description = description,
                UserType = type

            };
            return dbWorker.UserRepository.SaveUser(user);
        }

        public Guid Login(string userName)
        {
            return dbWorker.UserRepository.Login(userName);
        }

        public IEnumerable<ProductMapper> GetAllProducts()
        {
            return dbWorker.ProductRepository.GetAll();
        }

        public IEnumerable<ProductMapper> GetProducts(string userId)
        {
            Guid gUserId = new Guid(userId);

            return dbWorker.ProductRepository.GetByUserId(gUserId);
        }

        public ProductMapper GetProduct(string productId)
        {
            Guid gProductId = new Guid(productId);

            return dbWorker.ProductRepository.GetById(gProductId);
        }

        public string UploadFile(Stream file)
        {
            return SaveFile(file);
        }

        public Guid AddProduct(string name, string description, double voluem, double price, string unitType, string photoPath, string vendorId)
        {
            UnitEnum type = (UnitEnum)Enum.Parse(typeof(UnitEnum), unitType);
            Guid gVendorId = new Guid(vendorId);

            ProductMapper product = new ProductMapper
            {
                Title = name,
                Description = description,
                Cost = price,
                Unit = type,
                Value = voluem,
                Img = photoPath,
                OwnerId = gVendorId
            };

            return dbWorker.ProductRepository.AddProduct(product);
        }

        private string SaveFile(Stream file)
        {
            string appPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            string fileName = $"{ Path.GetRandomFileName() }.jpeg";
            string filePath = $"{appPath}Files\\{fileName}";
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                byte[] arrRead = new byte[10000];
                int bytesRead = 0;
                do
                {
                    bytesRead = file.Read(arrRead, 0, arrRead.Length);
                    fs.Write(arrRead, 0, bytesRead);
                } while (bytesRead > 0);

            }

            return $"/Files/{fileName}";
        }

        ////http://localhost:50409/APIService.svc/GetAllMessage
        //public IEnumerable<ItemMessage> GetAllMessage()
        //{
        //    return dbWorker.ChatRepository.GetAllMessage();
        //}

        ////http://localhost:50409/APIService.svc/SendMessage
        //public void SendMessage(string userName, string message)
        //{
        //    ItemMessage itemMessage = new ItemMessage
        //    {
        //        UserName = userName,
        //        Message = message
        //    };
        //    dbWorker.ChatRepository.AddMessage(itemMessage);
        //    //test
        //}
    }
}
