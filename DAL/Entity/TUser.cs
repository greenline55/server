﻿using DAL.Model.Enums;
using System;
using System.Collections.Generic;

namespace DAL.Entity
{
    public class TUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public TCoord Coord { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string Mail { get; set; }
        public string Description { get; set; }
        public UserTypeEnum UserType { get; set; }

        public List<TProduct> Products { get; set; } = new List<TProduct>();
    }
}
