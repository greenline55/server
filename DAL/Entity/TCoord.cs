﻿using System.Runtime.Serialization;

namespace DAL.Entity
{
    [DataContract]
    public class TCoord
    {
        [DataMember]
        public string X { get; set; }

        [DataMember]
        public string Y { get; set; }
    }
}
