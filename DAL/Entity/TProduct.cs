﻿using DAL.Model.Enums;
using System;

namespace DAL.Entity
{
    public class TProduct
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public UnitEnum Unit { get; set; }
        public double Voluem { get; set; }
        public string Photo { get; set; }

        public TUser Vendor { get; set; }
    }
}
