﻿using System;

namespace DAL.Entity
{
    //Заявка
    public class TRequest
    {
        public Guid Id { get; set; }
        public TUser User { get; set; }
        public string Message { get; set; }
    }
}
