﻿using DAL.Entity;
using DAL.Model.Enums;
using System.Collections.Generic;

namespace DAL.AppDataContext
{
    public class DataContext
    {
        public List<TUser> Users { get; set; } = new List<TUser>();
        public List<TProduct> Products { get; set; } = new List<TProduct>();
        public List<TRequest> Messages { get; set; } = new List<TRequest>();

        private static readonly DataContext instance = new DataContext();
        public static DataContext Instance
        {
            get
            {
                return instance;
            }
        }

        private DataContext() {
            InitData();
        }


        private void InitData()
        {
            TUser magaz1 = new TUser {Id=new System.Guid("05318086-ef13-4825-a523-70d162c09851"), Name = "Магнит", Phone = "55-36-00", Address="пр. Мира 32", Description="Предлагаем товары народного потребления",UserType=Model.Enums.UserTypeEnum.Retailer };
            TUser proizvoditel1 = new TUser { Id = new System.Guid("05318086-ef13-4825-a523-70d162c09852"), Name = "Крутые надои", Phone = "56-73-55", Address = "с. Полежайкино, ул. 1-я Разгульная, 77", Description = "Выдаиваем коров полностью! Первое место в облостном конкурсе голосистых коров.", UserType = Model.Enums.UserTypeEnum.Supplier };
            TUser proizvoditel2 = new TUser { Id = new System.Guid("05318086-ef13-4825-a523-70d162c09853"), Name = "Порванные подковы", Phone = "56-73-55", Address = "с. Ухабино, ул Покочкино, 34", Description = "Похвальная грамота от губернатора за метание валенков через забор на святки", UserType = Model.Enums.UserTypeEnum.Supplier };
            TProduct product1 = new TProduct { Id = new System.Guid("05318086-ef13-4825-a523-70d162c09854"), Name = "Сметана твердая", Description = "Самая жирная сметана, жирное 120%, ложка стоит, как мечь Артура в камне.", Price = 50, Unit = UnitEnum.Liter, Voluem = 0.9f, Photo = "/Files/sourcream.jpg" };
            TProduct product2 = new TProduct { Id = new System.Guid("05318086-ef13-4825-a523-70d162c09855"), Name = "Сыр класса 3Б", Description = "Натуральный сыр Класса 3Б: без ГМО, без примесей металла, без базара.", Price = 370, Unit = UnitEnum.Kilogram, Voluem = 1, Photo = "/Files/chees.jpg" };
            TProduct product3 = new TProduct { Id = new System.Guid("05318086-ef13-4825-a523-70d162c09856"), Name = "Картофель божественный", Description = "Картофель намоленный. С благославения местного дьяка Антипа закапывается. Для отпугивания вредителей церковныый хор обходит картофельные поля с песнопениями. После выкапывания урожай освещается ", Price = 8, Unit = UnitEnum.Kilogram, Voluem = 1f, Photo = "/Files/potato.jpg" };

            Users.Add(magaz1);

            proizvoditel1.Products.Add(product1);
            proizvoditel1.Products.Add(product2);
            product1.Vendor = proizvoditel1;
            product2.Vendor = proizvoditel1;
            Users.Add(proizvoditel1);

            proizvoditel2.Products.Add(product3);
            product3.Vendor = proizvoditel2;
            Users.Add(proizvoditel2);

        }
    }
}
