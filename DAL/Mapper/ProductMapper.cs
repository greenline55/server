﻿using DAL.Model.Enums;
using System;
using System.Runtime.Serialization;

namespace DAL.Entity
{
    [DataContract]
    public class ProductMapper
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public double Cost { get; set; }

        [DataMember]
        public UnitEnum Unit { get; set; }

        [DataMember]
        public double Value { get; set; }

        [DataMember]
        public string Img { get; set; }

        [DataMember]
        public Guid OwnerId { get; set; }

        [DataMember]
        public TCoord Coord { get; set; }

    }
}
