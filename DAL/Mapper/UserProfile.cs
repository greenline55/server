﻿using DAL.Model.Enums;
using System;
using System.Runtime.Serialization;

namespace DAL.Mapper
{
    [DataContract]
    public class UserProfile
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string CoordX { get; set; }

        [DataMember]
        public string CoordY { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Mail { get; set; }

        [DataMember]
        public string Skype { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public UserTypeEnum UserType { get; set; }
    }
}
