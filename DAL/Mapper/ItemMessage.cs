﻿using System.Runtime.Serialization;

namespace DAL.Entity
{
    [DataContract]
    public class ItemMessage
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
