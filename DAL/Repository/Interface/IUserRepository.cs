﻿using DAL.Entity;
using DAL.Mapper;
using System;

namespace DAL.Repository.Interface
{
    public interface IUserRepository
    {
        UserProfile GetUser(Guid userId);
        Guid SaveUser(TUser user);
        Guid Login(string userName);
    }
}
