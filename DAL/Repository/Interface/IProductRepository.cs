﻿using DAL.Entity;
using System;
using System.Collections.Generic;

namespace DAL.Repository.Interface
{
    public interface IProductRepository
    {
        IEnumerable<ProductMapper> GetByUserId(Guid userId);
        IEnumerable<ProductMapper> GetAll();
        ProductMapper GetById(Guid id);
        Guid AddProduct(ProductMapper product);
    }
}
