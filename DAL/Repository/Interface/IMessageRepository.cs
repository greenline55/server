﻿using DAL.Entity;
using System.Collections.Generic;

namespace DAL.Repository.Interface
{
    public interface IMessageRepository
    {
        IEnumerable<ItemMessage> GetAllMessage();
        void AddMessage(ItemMessage message);
    }
}
