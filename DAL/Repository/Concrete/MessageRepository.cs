﻿using DAL.AppDataContext;
using DAL.Entity;
using DAL.Repository.Interface;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repository.Concrete
{
    public class MessageRepository : IMessageRepository
    {
        private DataContext dataContext;
        public MessageRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IEnumerable<ItemMessage> GetAllMessage()
        {
            return dataContext.Messages.Select(m => new ItemMessage { UserName = m.User.Name, Message = m.Message });
        }

        public void AddMessage(ItemMessage message)
        {
            //TUser tUser = dataContext.Users.FirstOrDefault(u => u.Name.Equals(message.UserName));
            //TRequest tMessage = new TRequest { Message = message.Message };

            //if (tUser == null)
            //{
            //    tUser = new TUser { Name = message.UserName };
            //    dataContext.Users.Add(tUser);
            //}

            //tMessage.User = tUser;
            //tUser.Messages.Add(tMessage);
            //dataContext.Messages.Add(tMessage);
        }

    }
}
