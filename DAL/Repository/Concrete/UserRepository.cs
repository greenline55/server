﻿using DAL.AppDataContext;
using DAL.Entity;
using DAL.Mapper;
using DAL.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repository.Concrete
{
    public class UserRepository : IUserRepository
    {
        private DataContext dataContext;
        public UserRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public UserProfile GetUser(Guid userId)
        {
            return dataContext.Users
                .Where(u => u.Id.Equals(userId))
                .Select(u => new UserProfile
                {
                    Id = u.Id,
                    Name = u.Name,
                    Address = u.Address,
                    Phone = u.Phone,
                    Mail = u.Mail,
                    Skype = u.Skype,
                    CoordX = u.Coord.X,
                    CoordY = u.Coord.Y,
                    Description = u.Description,
                    UserType = u.UserType
                })
                .FirstOrDefault();
        }

        public Guid Login(string userName)
        {
            return dataContext.Users
                .Where(u => u.Name == userName)
                .Select(u => u.Id)
                .FirstOrDefault();
        }

        public Guid SaveUser(TUser user)
        {

            user.Id = Guid.NewGuid();
            dataContext.Users.Add(user);

            return user.Id;
        }

        //public IEnumerable<ItemMessage> GetAllMessage()
        //{
        //    return dataContext.Messages.Select(m => new ItemMessage { UserName = m.User.Name, Message = m.Message });
        //}

        //public void AddMessage(ItemMessage message)
        //{
        //    TUser tUser = dataContext.Users.FirstOrDefault(u => u.Name.Equals(message.UserName));
        //    TMessage tMessage = new TMessage { Message = message.Message };

        //    if (tUser == null)
        //    {
        //        tUser = new TUser { Name = message.UserName };
        //        dataContext.Users.Add(tUser);
        //    }

        //    tMessage.User = tUser;
        //    tUser.Messages.Add(tMessage);
        //    dataContext.Messages.Add(tMessage);
        //}

    }
}
