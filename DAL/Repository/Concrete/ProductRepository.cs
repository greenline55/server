﻿using DAL.AppDataContext;
using DAL.Entity;
using DAL.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repository.Concrete
{
    public class ProductRepository : IProductRepository
    {
        private DataContext dataContext;
        public ProductRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IEnumerable<ProductMapper> GetAll()
        {
            IEnumerable<ProductMapper> res = new List<ProductMapper>();

            IEnumerable<TProduct> products = dataContext.Products;

            res = ProductMapper(products);
            return res;
         
        }

        public IEnumerable<ProductMapper> GetByUserId(Guid userId)
        {
            IEnumerable<ProductMapper> res = new List<ProductMapper>();

            IEnumerable<TProduct> products = dataContext.Products
                .Where(p => p.Vendor.Id.Equals(userId));

            res = ProductMapper(products);
            return res;
            //return dataContext.Products
            //    .Where(p => p.Vendor.Id.Equals(userId))
            //    .Select(p => new ProductMapper
            //    {
            //        Id=p.Id,
            //        Title=p.Name,
            //        Description=p.Description,
            //        Value=p.Voluem,
            //        Cost=p.Price,
            //        Unit=p.Unit,
            //        Img=p.Photo,
            //        OwnerId=p.Vendor.Id,
            //    });
        }

        public ProductMapper GetById(Guid id)
        {
            ProductMapper res = null;

            ProductMapper product= dataContext.Products
                .Where(p => p.Id.Equals(id))
                .Select(p => new ProductMapper
                {
                    Id = p.Id,
                    Title = p.Name,
                    Description = p.Description,
                    Value = p.Voluem,
                    Cost = p.Price,
                    Unit = p.Unit,
                    Img = p.Photo,
                    OwnerId = p.Vendor.Id,
                })
                .FirstOrDefault();

            if (product == null) return res;

            TUser user = dataContext.Users
                            .Where(u => u.Id.Equals(product.OwnerId))
                            .FirstOrDefault();

            if (user == null) return res;

            product.Coord = new TCoord { X = user.Coord.X, Y = user.Coord.Y };

            res = product;

            return res;
        }

        public Guid AddProduct(ProductMapper product)
        {
            TUser tUser = dataContext.Users.FirstOrDefault(u => u.Id.Equals(product.OwnerId));
            if (tUser == null) return Guid.Empty;

            TProduct tProduct = new TProduct {
                Id = Guid.NewGuid(),
                Name = product.Title,
                Description = product.Description,
                Voluem = product.Value,
                Price = product.Cost,
                Unit = product.Unit,
                Photo = product.Img,
                Vendor = tUser,
            };

            tUser.Products.Add(tProduct);
            dataContext.Products.Add(tProduct);

            return tProduct.Id;
        }

        private IEnumerable<ProductMapper> ProductMapper(IEnumerable<TProduct> products)
        {
            foreach(var product in products)
            {
                ProductMapper res = new ProductMapper
                {
                    Id = product.Id,
                    Title = product.Name,
                    Description = product.Description,
                    Value = product.Voluem,
                    Cost = product.Price,
                    Unit = product.Unit,
                    Img = product.Photo,
                    OwnerId = product.Vendor.Id,
                };

                TUser user = dataContext.Users
                .Where(u => u.Id.Equals(res.OwnerId))
                .FirstOrDefault();

                res.Coord = new TCoord { X = user.Coord.X, Y = user.Coord.Y };

                yield return res;
            }
        }
       
    }
}
