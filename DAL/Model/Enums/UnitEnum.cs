﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace DAL.Model.Enums
{
    [DataContract]
    public enum UnitEnum
    {
        [EnumMember]
        None =0,

        [EnumMember]
        [Description("Литр")]
        Liter =1,

        [EnumMember]
        [Description("Килограмм")]
        Kilogram =2
    }
}
