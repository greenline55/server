﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace DAL.Model.Enums
{
    [DataContract]
    public enum UserTypeEnum
    {
        [EnumMember]
        None = 0,

        [EnumMember]
        [Description("Поставщик")]
        Supplier = 1,


        [EnumMember]
        [Description("Магазин")]
        Retailer = 2,
    }
}
